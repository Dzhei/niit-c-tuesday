#include <stdlib.h>
#include <string.h>
#include <stdio.h>

struct SYM // ������������� �������
{
    unsigned char ch; // ASCII-���
    unsigned int freq; // ������� �������������
    char code[256]; // ������ ��� ������ ����
    struct SYM *left; // ����� ������� � ������
    struct SYM *right; // ������ ������� � ������
};

typedef struct SYM TSYM;
typedef TSYM* PSYM;


union CODE {
    unsigned char ch;
    struct {
        unsigned short b1:1;
        unsigned short b2:1;
        unsigned short b3:1;
        unsigned short b4:1;
        unsigned short b5:1;
        unsigned short b6:1;
        unsigned short b7:1;
        unsigned short b8:1;
    } byte;
};


PSYM buildTree(PSYM psym[], int N);
void makeCodes(PSYM root);
void unpack(char *buf);
long decode(FILE *fp_o, FILE * fp, PSYM root, long len);