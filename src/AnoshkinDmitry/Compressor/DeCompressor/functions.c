#include "functions.h"

PSYM buildTree(PSYM psym[], int N)
{
    // ������ ��������� ����
    PSYM temp=(PSYM)malloc(sizeof(TSYM));
    int i;
    // � ���� ������� ������������ ����� ������
    // ���������� � �������������� ��������� ������� psym
    temp->freq=psym[N-2]->freq+psym[N-1]->freq;
    // ��������� ��������� ���� � ����� ���������� ������
    temp->left=psym[N-1];
    temp->right=psym[N-2];
    temp->code[0]=0;
    if(N==2) // �� ������������ �������� ������� � �������� 1.0
        return temp;
    else
    {
        // ��������� temp � ������ ������� psym,
        // �������� ������� �������� �������
        i=N-2;
        while(temp->freq>psym[i]->freq)
        {
            psym[i+1]=psym[i];
            i--;
            if (i<0)
                break;
        }
        psym[i+1]=temp;
    }
    return buildTree(psym,N-1);
}

void makeCodes(PSYM root)
{
    if(root->left)
    {
        strcpy(root->left->code,root->code);
        strcat(root->left->code,"0");
        makeCodes(root->left);
    }
    if(root->right)
    {
        strcpy(root->right->code,root->code);
        strcat(root->right->code,"1");
        makeCodes(root->right);
    }
}

void unpack(char * buf)
{
    union CODE code;
    code.ch=buf[0];

    buf[7]=code.byte.b8+'0';
    buf[1]=code.byte.b2+'0';
    buf[2]=code.byte.b3+'0';
    buf[3]=code.byte.b4+'0';
    buf[4]=code.byte.b5+'0';
    buf[5]=code.byte.b6+'0';
    buf[6]=code.byte.b7+'0';
    buf[0]=code.byte.b1+'0';
    buf[8]=0;
}
long decode(FILE *fp_o, FILE * fp, PSYM root)
{
    char tmp;
    long count=0;
    
    if(!root->right && !root->left){
        fwrite(&root->ch, sizeof(char), 1, fp);
        return 1;
    }
    else{
        fread(&tmp, sizeof(char),1, fp_o);
        if(tmp=='1')
            return decode(fp_o, fp, root->right);
        else
            return decode(fp_o, fp, root->left);
    }
    return count;
}