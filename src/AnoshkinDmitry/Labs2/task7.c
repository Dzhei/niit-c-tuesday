/*
7. �������� ���������, ��������� ������� ������������� �������� ��� ������-
��� ������������� ������. � ���� ������� ���������� ������ ������ � �����
��� ����������.
���������:
� ���� ��������� �� ��������� � �������� �������, ��� ��� �������������
�������������� �������� ���������.
*/

// �������� ������� �.�.


#include <stdio.h>

#define MaxLen 100

int main()
{
    char str=0;
    int count [128] = {0};
    int i=0;

    puts ("Enter your string:\n");
    do
    {
        str=getchar();
        count [str]++;
        i++;
        if (i>MaxLen)
        {
            puts ("String too long!");
            return 0;
        }
    }
    while (str!=10);
    for (i=11; i<128; i++)
        if (count[i] !=0)
            printf ("%c - %d \n", i, count[i]);

    return 0;
}
