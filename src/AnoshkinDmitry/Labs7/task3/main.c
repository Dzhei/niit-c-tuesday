/*
 �������� ���������, ������� ������ ������� ������������� ���-
����� ��� ������������� �����, ��� �������� ������� � ������-
��� ������. ��������� ������ �������� �� ����� ������� �����-
��������, ��������������� �� �������� �������
���������:
� ��������� ���������� ���������� ����������� ��� SYM, � ������� �����
������� ��� ������� � ������� ������������� (������������ ����� �� 0 �� 1).
����� ������� �����, ������ �������� SYM ������ ���� ������������ ��
�������.
*/

//by Anoshkin Dmitry

#include "Symbols.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[])
{
    FILE *fp;
    TSYM *Arr;
    int i;

    if (argc!=2) 
    {
        puts ("Input Error!");
        exit (1);
    }
    fp=fopen(*(argv+1), "rt");
    if (!fp)
    {
        perror("file:");
        exit (1);
    }
    Arr=CreateArr(fp);
    for(i=0;i<256; i++)
        if ((Arr+i)->freq>0)
            printf("%c - %0.4f%% \n", (Arr+i)->sym, (Arr+i)->freq*100);

    return 0;
}