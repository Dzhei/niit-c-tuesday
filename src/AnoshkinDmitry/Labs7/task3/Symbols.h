#include <stdio.h>
#include <stdlib.h>

struct SYM
{
    char sym;
    float freq;
};
typedef struct SYM TSYM;

void ReNew(TSYM ** Arr, int count, char symbol);
TSYM * CreateArr(FILE *fp);
int cmp(const void *a, const void *b);
