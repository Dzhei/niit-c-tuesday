/*
�������� ���������, ������� ��������� �� ������������ ������ �
������� �� �� �����, ��������� ����� � ��������� �������.
���������:
��������� ������ �������� ������� �� ���� �������:
a) printWord - ������� ����� �� ������ (�� ����� ������ ��� �������)
b) getWords - ��������� ������ ���������� �������� ������ ���� ����
c) main - �������� �������
*/

//by Anoshkin Dmitry

#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#define MaxLen 250

void printWord (char*pstn)
{
    int i=0;
    while (*(pstn+i)!=' '&&(*(pstn+i))!='\n')
        putchar(*(pstn+i++));
    putchar (' ');
    return 0;
}

int getWords (char*str, char**pstn, int len)
{
    int count=0;
    int i=0;
    while(*(str+i)!=10)
    {
        while(*(str+i)==' ') i++;
        if (*(str+i)!=' '&& *(str+i)!=10)
        {
            pstn[count]=str+i;
            count++;
            while(*(str+i)!=' '&&*(str+i)!=10) i++;
        }
    }
    return count;
}

int main ()
{
    char str[MaxLen] = {0};
    char*pstn[MaxLen/2]={0};
    int i=0, j=0;
    int count=0, value=0;
    puts ("Input your string");
    fgets (str, MaxLen, stdin);
    count=getWords (str, pstn, MaxLen/2);
    srand(time(0));
    for (;i<count;i++)
    {
        value=rand()%(count-i);
        printWord (pstn[value]);
        for (j=value; j<count-1; j++)
            pstn[j]=pstn[j+1];
    }
    return 0;
}
