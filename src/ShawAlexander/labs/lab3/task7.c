/*
7. �������� ���������, ������� �������� ������� ������������� �������� ���
�������� ������, ��������������� �� �������� �������
���������:
������� ��������� ����� �������, ��� ������� ���� ����� ��������������
�������, � ����� �� ���� ��������.
*/

#include <stdio.h>
#include <string.h>
#define SIZE 2001 //������ �������
int main() {
	char string[SIZE];
	puts("Enter a string:");
	fgets(string, SIZE, stdin);
	char chars[SIZE] = { 0 };
	int amount[SIZE] = { 0 };
	//������ �������� � chars, � ����� ���������� - � amount; chars[0] ����� ����� ����������, ���������� � amount[0] � �.�.
	for (int i = 0, currentPos = 0, existingPos = 0, exists = 0; i < strlen(string); i++) {
		char c = string[i];
		if (c != '\n' && c != ' ' && c != '\t') {
			for (int x = 0; x < strlen(chars); x++) { //���������, ��� �� ������� � ������� chars
				if (chars[x] == c) {
					exists = 1;
					existingPos = x;
					break;
				}
			}
			if (exists == 0) {
				chars[currentPos] = c;
				amount[currentPos] = 1;
				currentPos++;
			}
			else {
				amount[existingPos]++;
				exists = 0;
			}
		}
	}
	//����������� ���������� �� ��������
	for (int i = SIZE; i > 0; i--) {
		for (int q = 0; q < i; q++)
		{
			if (amount[q] < amount[q + 1]) {
				int tmp = amount[q];
				amount[q] = amount[q + 1];
				amount[q + 1] = tmp;
				char tmp2 = chars[q];
				chars[q] = chars[q + 1];
				chars[q + 1] = tmp2;
			}
		}
	}
	//��������
	for (int i = 0; i < strlen(chars); i++) {
		printf("%c : %d\n", chars[i], amount[i]);
	}
	return 0;
}